import 'package:flutter/material.dart';
import 'package:slow_flutter_app/src/lorem_ipsum.dart';

class PurplePage extends StatelessWidget {
  const PurplePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.purple,
      child: Padding(
        padding: const EdgeInsets.only(top: 180, left: 12, right: 12),
        child: PhysicalShape(
          color: Colors.white,
          clipBehavior: Clip.antiAlias,
          clipper: const ShapeBorderClipper(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(16),
                topRight: Radius.circular(16),
              ),
            ),
          ),
          elevation: 18,
          child: ListView(
            padding: const EdgeInsets.all(16),
            itemExtent: 30,
            children: [
              for (var line in loremIpsum) Text(line),
            ],
          ),
        ),
      ),
    );
  }
}
