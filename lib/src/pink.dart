import 'package:flutter/material.dart';
import 'package:slow_flutter_app/src/rounded_card.dart';

class PinkPage extends StatefulWidget {
  const PinkPage({super.key});

  @override
  _PinkPageState createState() => _PinkPageState();
}

class _PinkPageState extends State<PinkPage> {
  BigInt? _result = BigInt.zero;

  @override
  Widget build(BuildContext context) {
    return MyRoundedCard(
      color: Colors.pink,
      child: Column(
        children: <Widget>[
          TextButton(
            onPressed: _startComputation,
            style: ButtonStyle(
                backgroundColor:
                    MaterialStateColor.resolveWith((_) => Colors.pink)),
            child: const Text('COMPUTE', style: TextStyle(color: Colors.white)),
          ),
          const SizedBox(height: 100),
          const Text('Result'),
          const SizedBox(height: 20),
          _result == null
              ? const CircularProgressIndicator()
              : Text(
                  _result.toString(),
                  style: const TextStyle(fontSize: 40),
                ),
        ],
      ),
    );
  }

  Future<void> _startComputation() async {
    setState(() {
      _result = null;
    });
    await Future.delayed(const Duration(milliseconds: 200));
    var result = _fibonacci(BigInt.from(32));
    await Future.delayed(const Duration(milliseconds: 200));
    setState(() {
      _result = result;
    });
  }
}

BigInt _fibonacci(BigInt n) {
  if (n <= BigInt.from(2)) return BigInt.one;
  return _fibonacci(n - BigInt.one) + _fibonacci(n - BigInt.two);
}
