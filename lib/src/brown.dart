import 'package:flutter/material.dart';
import 'package:slow_flutter_app/src/lorem_ipsum.dart';
import 'package:slow_flutter_app/src/rounded_card.dart';

class BrownPage extends StatelessWidget {
  const BrownPage({super.key});

  @override
  Widget build(BuildContext context) {
    return MyRoundedCard(
      color: Colors.brown,
      child: ListView(
        children: const [
          ShadowBonanza(index:1),
          ShadowBonanza(index:2),
          ShadowBonanza(index:3),
          ShadowBonanza(index:4),
          ShadowBonanza(index:5),
          ShadowBonanza(index:6),
          ShadowBonanza(index:7),
          ShadowBonanza(index:8),
          ShadowBonanza(index:9),
          ShadowBonanza(index:10),
          ShadowBonanza(index:11),
          ShadowBonanza(index:12),
        ],
      ),
    );
  }
}

class ShadowBonanza extends StatelessWidget {
  final int index;

  const ShadowBonanza({super.key, required this.index});


  @override
  Widget build(BuildContext context) {
    var line = loremIpsum[index % loremIpsum.length];
    var color1 = Colors.primaries[index % Colors.primaries.length];
    var color2 = Colors.primaries[(index + 10) % Colors.primaries.length];
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 32),
      child: Row(
        children: <Widget>[
          Material(
            elevation: 12,
            color: color1,
            shadowColor: color1,
            child: SizedBox(
              width: 30,
              height: 30,
              child: Center(
                child: Text(
                  line.substring(0, 1),
                  style: const TextStyle(color: Colors.white),
                ),
              ),
            ),
          ),
          const SizedBox(width: 10),
          Expanded(
            child: Text(
              line,
              style: TextStyle(
                shadows: [
                  Shadow(
                    color: color1,
                    offset: const Offset(3, 4),
                    blurRadius: 8,
                  ),
                ],
              ),
              softWrap: false,
            ),
          ),
          const SizedBox(width: 10),
          Material(
            elevation: 12,
            color: color2,
            shadowColor: color2,
            type: MaterialType.circle,
            child: SizedBox(
              width: 30,
              height: 30,
              child: Center(
                child: Text(
                  line.substring(0, 1),
                  style: const TextStyle(color: Colors.white),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
