import 'package:flutter/material.dart';

class MyRoundedCard extends StatelessWidget {
  final Color color;

  final Widget child;

  const MyRoundedCard({super.key, required this.color, required this.child});

    

  @override
  Widget build(BuildContext context) {
    return Container(
      color: color,
      child: Padding(
        padding: const EdgeInsets.only(top: 180, left: 12, right: 12),
        child: PhysicalShape(
          color: Colors.white,
          clipper: const ShapeBorderClipper(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(16),
                topRight: Radius.circular(16),
              ),
            ),
          ),
          elevation: 18,
          child: child,
        ),
      ),
    );
  }
}
