import 'package:flutter/material.dart';
import 'package:slow_flutter_app/src/rounded_card.dart';

class AmberPage extends StatelessWidget {
  const AmberPage({super.key});

  @override
  Widget build(BuildContext context) {
    // ignore: prefer_const_constructors
    return  MyRoundedCard(
      color: Colors.amber,
      child: const Padding(
        padding: EdgeInsets.all(32),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            FlutterLogo(),
            Padding(
              padding: EdgeInsets.all(4),
              child: Text(_flutterBlurb),
            ),
          ],
        ),
      ),
    );
  }
}

const _flutterBlurb =
    'Flutter is Google’s UI toolkit for building beautiful, natively compiled applications for mobile, web, and desktop from a single codebase.';
