import 'package:flutter/material.dart';
import 'package:slow_flutter_app/src/rounded_card.dart';

class BlackPage extends StatelessWidget {
  const BlackPage({super.key});

  @override
  Widget build(BuildContext context) {
    // ignore: prefer_const_constructors
    return  MyRoundedCard(
      color: Colors.black,
      child: const Row(
        children: <Widget>[
          Expanded(
            child: MyPerception(label:'A',curve: Curves.linear),
          ),
          Expanded(
            child: MyPerception(label:'B',curve: Curves.easeOutCubic),
          ),
        ],
      ),
    );
  }
}

class MyPerception extends StatefulWidget {
  final String label;

  final Curve curve;

  const MyPerception({super.key, required this.label, required this.curve});

  

  @override
  _MyPerceptionState createState() => _MyPerceptionState();
}

class _MyPerceptionState extends State<MyPerception> {
  bool _goingUp = true;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        MaterialButton(
          onPressed: () => setState(() => _goingUp = !_goingUp),
          child: Text(widget.label),
        ),
        Expanded(
          child: TweenAnimationBuilder(
            tween: AlignmentTween(
              begin: _goingUp ? Alignment.bottomCenter : Alignment.topCenter,
              end: _goingUp ? Alignment.topCenter : Alignment.bottomCenter,
            ),
            duration: const Duration(milliseconds: 300),
            curve: widget.curve,
            builder: (context, alignment, child) {
              return Align(
                alignment: alignment,
                child: child,
              );
            },
            child: Container(
              width: 50,
              height: 50,
              color: Colors.black,
            ),
          ),
        ),
        const SizedBox(height: 50),
      ],
    );
  }
}
