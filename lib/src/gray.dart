import 'package:flutter/material.dart';

class GrayPage extends StatelessWidget {
  const GrayPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [Colors.blueAccent, Colors.blueGrey],
        ),
      ),
    );
  }
}

class LightGrayPage extends StatelessWidget {
  const LightGrayPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [Colors.black12, Colors.grey],
        ),
      ),
    );
  }
}
