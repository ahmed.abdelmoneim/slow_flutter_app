import 'package:flutter/material.dart';
import 'package:slow_flutter_app/src/amber.dart';
import 'package:slow_flutter_app/src/black.dart';
import 'package:slow_flutter_app/src/blue.dart';
import 'package:slow_flutter_app/src/brown.dart';
import 'package:slow_flutter_app/src/gray.dart';
import 'package:slow_flutter_app/src/green.dart';
import 'package:slow_flutter_app/src/indigo.dart';
import 'package:slow_flutter_app/src/lime.dart';
import 'package:slow_flutter_app/src/orange.dart';
import 'package:slow_flutter_app/src/pink.dart';
import 'package:slow_flutter_app/src/welcome.dart';
import 'package:slow_flutter_app/src/yellow.dart';


void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  const MyHomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        children: [
         const  WelcomePage(),
       const   LightGrayPage(),
          GrayPage(),
          AmberPage(),
          LightGrayPage(),
          GrayPage(),
          YellowPage(),
          LightGrayPage(),
          GrayPage(),
          GreenPage(),
          LightGrayPage(),
          GrayPage(),
          BluePage(),
          LightGrayPage(),
          GrayPage(),
          OrangePage(),
          LightGrayPage(),
          GrayPage(),
          PinkPage(),
          LightGrayPage(),
          GrayPage(),
          BrownPage(),
          // LightGrayPage(),
          // GrayPage(),
          // PurplePage(),
          LightGrayPage(),
          GrayPage(),
          IndigoPage(),
          LightGrayPage(),
          GrayPage(),
          LimePage(),
          LightGrayPage(),
          GrayPage(),
          BlackPage(),
        ],
      ),
    );
  }
}
